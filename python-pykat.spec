# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python3_sitelib: %global python3_sitelib %(%{__python3} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python3_sitearch: %global python3_sitearch %(%{__python3} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

%global srcname PyKat
%global lsrcname pykat

Name:           python-pykat
Version:        1.2.2
Release:        1%{?dist}
Summary:        Python wrapper to the finesse utility

License:        GPLv2
URL:            https://pypi.python.org/pypi/PyKat
# git archive --format tar.gz -o SOURCES/pykat-1.1.tar.gz --remote=git@git.ligo.org:finesse/pykat.git --prefix pykat-1.1/ tags/1.1
Source0:	https://files.pythonhosted.org/packages/d0/fc/5e08bede156f5a80ab701e6743c3b894e7433c5f2f8f24984390597bc372/PyKat-1.2.2.tar.gz

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-six
BuildRequires:  python%{python3_pkgversion}-numpy
BuildRequires:  python%{python3_pkgversion}-scipy
BuildRequires:  finesse >= 2.3

%description
PyKat is a wrapper for using FINESSE (http://www.gwoptics.org/finesse). It aims to provide a Python toolset for automating more complex tasks as well as providing a GUI for manipulating and viewing simulation setups.

%package -n python%{python3_pkgversion}-%{lsrcname}
Obsoletes: python-%{lsrcname}
Requires:  finesse
Summary:        Python wrapper to the finesse utility
%description -n  python%{python3_pkgversion}-%{lsrcname}
PyKat is a wrapper for using FINESSE (http://www.gwoptics.org/finesse). It aims to provide a Python toolset for automating more complex tasks as well as providing a GUI for manipulating and viewing simulation setups.

%prep
%setup -q -n %{srcname}-%{version}


%build
export FINESSE_DIR=%{_bindir}
export KATINI=%{_sysconfdir}/kat.ini
# Remove CFLAGS=... for noarch packages (unneeded)
CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build


%install
export FINESSE_DIR=%{_bindir}
export KATINI=%{_sysconfdir}/kat.ini
rm -rf $RPM_BUILD_ROOT
%{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

 
%files -n  python%{python3_pkgversion}-%{lsrcname}
%doc
%{_bindir}/pykat
%{python3_sitelib}/*


%changelog
* Thu Dec 12 2019 Michael Thomas <michael.thomas@LIGO.ORG> 1.2.2-1
- Update to 1.2.2

* Thu Dec 12 2019 Michael Thomas <michael.thomas@LIGO.ORG> 1.1.386-1
- Update to 1.1.386

* Wed Oct 30 2019 Michael Thomas <michael.thomas@LIGO.ORG> 1.1.380-1
- Update to 1.1.380

* Fri Oct 25 2019 Michael Thomas <michael.thomas@LIGO.ORG> 1.1.372-2
- Fix package naming

* Mon Sep 9 2019 Michael Thomas <michael.thomas@LIGO.ORG> 1.1.372-1
- Initial package
